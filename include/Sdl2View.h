#ifndef SDL2VIEW_H
#define SDL2VIEW_H

#include "BaseService.h"
#include "ServiceManager.h"
#include "MenuManager.h"
#include "IMenuListener.h"

#include <SDL2/SDL.h>

class Sdl2View: public spm::BaseService,
        public spm::IMenuListener
{
public:

    explicit Sdl2View(spm::ServiceManager& manager);
    virtual ~Sdl2View();

private:
    // -------------------
    // PRIVATE TYPES
    // -------------------


    // -------------------
    // PRIVATE METHODS
    // -------------------
    // initialize method
    virtual void initialize() override final;
    // handle tick of loop
    virtual bool tick() override final;
    // handle action
    virtual void action(const std::string& type, const spm::Variant& data) override final;

    // open menu
    virtual void onOpenMenu(const spm::Menu* menu) override final;
    // close menu
    virtual void onCloseMenu() override final;
    // action menu
    virtual void onMenuAction(uint32_t action) override final;

    inline void cleanup();

    // -------------------
    // MEMORY DATA
    // -------------------
    spm::ServiceManager *m_manager;
    bool                 m_running;
    bool                 m_init;
    spm::MenuManager     m_menuManager;

    // SDL2
    SDL_Window          *m_window;
    SDL_Surface         *m_surface;
};

#endif // SDL2VIEW_H
