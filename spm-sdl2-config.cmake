option(BUILD_SPM_SDL2 "Build SDL2 graphics" OFF)

if(BUILD_SPM_SDL2)
    include_directories("${CMAKE_CURRENT_LIST_DIR}/include")
    add_definitions(-DSPM_SDL2)
endif()
