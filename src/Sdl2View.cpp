#include "Sdl2View.h"

Sdl2View::Sdl2View(spm::ServiceManager &manager):
    BaseService("sdl2_view"),
    m_manager(&manager),
    m_running(false),
    m_menuManager(this),
    m_init(false)
{
}

Sdl2View::~Sdl2View()
{
    cleanup();
}

void Sdl2View::initialize()
{
    if(SDL_Init(SDL_INIT_VIDEO) < 0)
        return;
    m_window = SDL_CreateWindow("SDL2", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 640,480, SDL_WINDOW_SHOWN);
    if(!m_window)
        return;
    m_surface = SDL_GetWindowSurface(m_window);
    if(!m_surface)
        return;


    m_init = true;
    m_running = true;
}

bool Sdl2View::tick()
{
    SDL_Event event;
    while(SDL_PollEvent(&event) != 0)
    {
        switch(event.type)
        {
        case SDL_KEYDOWN:
            if(event.key.keysym.sym == SDLK_RETURN)
            {
                m_running = false;
                m_manager->performAction("view", {{"event", "exit"}});
            }
            break;
        case SDL_QUIT:
            m_running = false;
            m_manager->performAction("view", {{"event", "exit"}});
            break;
        default:
            break;
        }
    }

    SDL_UpdateWindowSurface(m_window);

    return m_running;
}

void Sdl2View::action(const std::string &type, const spm::Variant &data)
{

}

void Sdl2View::onOpenMenu(const spm::Menu *menu)
{

}

void Sdl2View::onCloseMenu()
{

}

void Sdl2View::onMenuAction(uint32_t action)
{

}

void Sdl2View::cleanup()
{
    if(!m_init)
        return;
    SDL_DestroyWindow(m_window);
    SDL_Quit();
}
